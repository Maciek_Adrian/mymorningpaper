﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class AiAgentMovable : MonoBehaviour {
    private AIPath AIPath;
	        // Use this for initialization
    void Start () {
        AIPath = GetComponent<AIPath>();
	}

    public void SetMovable(bool value) {
        AIPath.canMove = value;
    }
}
