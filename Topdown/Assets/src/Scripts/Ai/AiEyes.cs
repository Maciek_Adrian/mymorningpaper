﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiEyes : MonoBehaviour
{
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private Transform Firepoint;


    [SerializeField] private Transform pfFieldOfView;
    private Transform fieldOfViewInstance;
    private FieldOfView fieldOfView;

    public float fov_angle;
    public float fov_distance;

    public Transform PlayerTransform;

    public bool GetIfSeesPlayer() {
        if (PlayerInFielOfViewRange()) {   
            if(PlayerRaycasted()) return true;
        }
        return false;
    }



    // Start is called before the first frame update
    void Start()
    {
        fieldOfViewInstance = Instantiate(pfFieldOfView, null);
        fieldOfView = fieldOfViewInstance.GetComponent<FieldOfView>();
        fieldOfView.SetFovAngle(fov_angle);
        fieldOfView.SetViewDistance(fov_distance);
    }

    // Update is called once per frame
    void Update()
    {
        fieldOfView.SetOrigin(transform.position);
        fieldOfView.SetAimDirection(transform.rotation.eulerAngles.z + 90);


    }

    public void DestroyFieldOfViewInstance()
    {
        Destroy(fieldOfViewInstance.gameObject);
    }

    public Vector3 GetAim()
    {
        return MathUtils.GetVectorFromAngle(transform.rotation.eulerAngles.z + 90);
    }

    private bool PlayerInFielOfViewRange()
    {
        if (PlayerTransform == null) return false;
        if (Vector3.Distance(PlayerTransform.position, transform.position) < fov_distance)
        {
            Vector3 dirToPlayer = (PlayerTransform.position - transform.position).normalized;
            if (Vector3.Angle(GetAim(), dirToPlayer) < fov_angle / 2)
            {
                Debug.Log("player in range");
                return true;
            }
        }
        return false;
    }

    public bool PlayerRaycasted()
    {
        if (PlayerTransform == null) return false;
        RaycastHit2D hitInfo = Physics2D.Raycast(Firepoint.position, PlayerTransform.position - Firepoint.position, fov_distance, layerMask);
        if (hitInfo)
        {
           
            if (hitInfo.transform.CompareTag("Player"))
            {
                Debug.Log("raycasted");
                return true;
            }
        }
        return false;

    }
}
