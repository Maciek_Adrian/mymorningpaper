﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckForFlyingBullets : MonoBehaviour {
    [SerializeField] private bool seeBullet= false; 

    public bool GetIfSeesBullet()
    {
        return seeBullet;

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("bullet")) {
            seeBullet = true;
        }
    }


}
