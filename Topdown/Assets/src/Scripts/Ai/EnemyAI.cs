﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EnemyAI : MonoBehaviour {

    [SerializeField] public GameObject Loot;
    public EnemyState CurrentState;
    public EnemyState StartingState;
    private Health Health;
    private int initialHealth;
    private Patrol PatrolScript;
    [SerializeField] private CheckForFlyingBullets CheckForFlyingBullets;
    [SerializeField] private AiEyes eyes;
    [SerializeField] private GunType gun;
    [SerializeField] private int attackDistance=10;
    private EnemyMeleeController EnemyMeleeController;
    [SerializeField] private GameObject MeleeWeapon;

    private AIDestinationSetter AIDestinationSetterScript;
    public GameObject bulletPrefab;
    public Transform FirePoint;
    public float bulletForce = 20f;

    public int AttackWalkSpeed = 5;
    public float attackReloadTime=0.5f;

    public GameObject Enemies;

	void Start () {
        CurrentState = StartingState;
        Health = GetComponent<Health>();
        initialHealth = Health.HealthPoints;
        PatrolScript = GetComponent<Patrol>();
        AIDestinationSetterScript = GetComponent<AIDestinationSetter>();
        ChangeState(CurrentState);
        EnemyMeleeController = GetComponent<EnemyMeleeController>();
        if (gun == GunType.MELEE)
        {
            MeleeWeapon.SetActive(true);
        }
	}


    public IEnumerator Patrol() {

        while (true) {

            if (initialHealth != Health.HealthPoints) {
                if(LessThenFiveFighting())
                ChangeState(EnemyState.ATTACK);
            }

            if (CheckForFlyingBullets.GetIfSeesBullet())
            {
                if (LessThenFiveFighting())
                    ChangeState(EnemyState.ATTACK);
            }

            if (eyes.GetIfSeesPlayer()) {
                if (LessThenFiveFighting())
                    ChangeState(EnemyState.ATTACK);
            }
            
            yield return new WaitForSeconds(0.1f);
        }

        yield return null;
    }

    public IEnumerator Attack() {
        gameObject.GetComponent<AIPath>().maxSpeed = AttackWalkSpeed;

        while (eyes.PlayerTransform != null) {
            Vector2 l = eyes.PlayerTransform.position - gameObject.transform.position;

            if (l.sqrMagnitude > 1000) {
                gameObject.GetComponent<AIPath>().maxSpeed = AttackWalkSpeed - 2;
                ChangeState(EnemyState.PATROL);
            }
            if (l.sqrMagnitude > attackDistance*attackDistance)
            {
                yield return new WaitForSeconds(0.1f);
            }
            if (eyes.GetIfSeesPlayer() && l.sqrMagnitude < attackDistance * attackDistance)
            {
                performAttack();
                yield return new WaitForSeconds(attackReloadTime);
            }
            else
            {
                Debug.Log("I dont see player");
                yield return new WaitForSeconds(0.1f);
            }
           
        }
        yield return null;
    }

    public IEnumerator Idle() {
        while (true)
        {
            if (initialHealth != Health.HealthPoints)
            {
                ChangeState(EnemyState.ATTACK);
            }

            if (CheckForFlyingBullets.GetIfSeesBullet())
            {
                ChangeState(EnemyState.ATTACK);
            }

            if (eyes.GetIfSeesPlayer())
            {
                ChangeState(EnemyState.ATTACK);
                
            }
            yield return new WaitForSeconds(0.1f);
        }

        yield return null;
    }

    public void ChangeState(EnemyState NewState) {
        StopAllCoroutines();

        CurrentState = NewState;

        switch (NewState)
        {
            case EnemyState.IDLE:
                StartCoroutine(Idle());
                break;
            case EnemyState.PATROL:
                AIDestinationSetterScript.enabled = false;
                PatrolScript.enabled = true;
                StartCoroutine(Patrol());
                break;
            case EnemyState.ATTACK:
                PatrolScript.enabled = false;
                AIDestinationSetterScript.enabled = true;
                StartCoroutine(Attack());
                break;
        }
    }

    private void performAttack()
    {
        switch (gun)
        {
            case GunType.HANDGUN:
                handgunShot();
                break;
            case GunType.SHOTGUN:
                shotgunShot();
                break;
            case GunType.MACHINEGUN:
                machinegunShot();
                break;
            case GunType.MELEE:
                EnemyMeleeController.Swing();
                break;

        }
    }

    public bool LessThenFiveFighting()
    {
        int sum = 0;
        for (int i = 0; i < Enemies.transform.childCount; i++)
        {
            if (Enemies.transform.GetChild(i).GetComponent<EnemyAI>().CurrentState == EnemyState.ATTACK)
                sum++;
        }

        if (sum < 5) return true;

        return false;
    }


    public AiEyes GetEyes() {
        return eyes;    
    }

    private void handgunShot()
    {

        GameObject bullet = Instantiate(bulletPrefab, FirePoint.position, FirePoint.rotation);

        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(FirePoint.right * bulletForce, ForceMode2D.Impulse);
    }

    private void shotgunShot()
    {

        for (int i = 0; i < 5; i++)
        {
            GameObject bullet = Instantiate(bulletPrefab, FirePoint.position + FirePoint.right * i / 4, FirePoint.rotation);
            bullet.transform.Rotate(new Vector3(0, 0, -10 + i * 5));

            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(bullet.transform.right * bulletForce, ForceMode2D.Impulse);
        }
    }

    private void machinegunShot()
    {
        GameObject bullet = Instantiate(bulletPrefab, FirePoint.position, FirePoint.rotation);

        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(FirePoint.right * bulletForce, ForceMode2D.Impulse);
    }
}
