﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListenAnswersWhenTalkedTo : MonoBehaviour {
    public GameEventListener ListenAns1;
    public GameEventListener ListenAns2;
    public GameEventListener ListenAns3;
    public GameEventListener ListenAns4;

    public DialogueHandler DialogueHandler;

    private bool areActive = false;
	
	// Update is called once per frame
	void Update () {
        if (DialogueHandler.Dialogue.DialogueInProgress) { 
            ListenAns1.enabled = true;
            ListenAns2.enabled = true;
            ListenAns3.enabled = true;
            ListenAns4.enabled = true;
        }else{
            ListenAns1.enabled = false;
            ListenAns2.enabled = false;
            ListenAns3.enabled = false;
            ListenAns4.enabled = false;
        }

    }
}
