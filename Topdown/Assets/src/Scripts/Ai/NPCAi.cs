﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class NPCAi : MonoBehaviour {

    public ShowDialogueOption ShowDialogueOption;
    public Patrol Patrol;
    AIPath AiPath;
    float startingSpeed;
    [SerializeField] private AiEyes eyes;
    [SerializeField] private DialogueHandler DialogueHandler;
    [SerializeField] private AIDestinationSetter aIDestinationSetter;

    private bool alreadyTalked = false;
    private void Start()
    {
        StartCoroutine("Walking");
        AiPath = GetComponent<AIPath>();
        startingSpeed = AiPath.maxSpeed;
    }

    public AiEyes GetEyes() {
        return eyes;
    }

    private  IEnumerator Walking() {
        while (true) {
            if (eyes.GetIfSeesPlayer() && !alreadyTalked) {
                alreadyTalked = true;
                Debug.Log("saw you");
                Patrol.enabled = false;
                aIDestinationSetter.enabled = true;
                DialogueHandler.PerformDialogue(); // pamietaj ze walking sie czesciej odpala niz perform 
            }
            else if (ShowDialogueOption.talkable && alreadyTalked && DialogueHandler.Dialogue.NotBusy)
            {
                Debug.Log("Npc");
                AiPath.maxSpeed = 0;
                aIDestinationSetter.enabled = false;
                Patrol.enabled = false;
                StopAllCoroutines();
            }
                yield return new WaitForSeconds(0.1f);
        }
    }





}
