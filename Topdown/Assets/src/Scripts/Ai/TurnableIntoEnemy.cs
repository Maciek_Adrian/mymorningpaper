﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class TurnableIntoEnemy : MonoBehaviour {
    public BoolVariable Condition;
    public GameObject Enemies;
    public GameObject EnemyInside;
    public GameObject Player;


    void Update()
    {
        if (Condition.Value)
        {
            GameObject enemy = Instantiate(EnemyInside);
            enemy.transform.position = gameObject.transform.position;
            enemy.SetActive(true);
            enemy.transform.parent = Enemies.transform;
            enemy.GetComponent<EnemyAI>().GetEyes().PlayerTransform = Player.transform.GetChild(0);
            enemy.GetComponent<AIDestinationSetter>().target = Player.transform.GetChild(0);
            Destroy(gameObject);
        }
    }
}
