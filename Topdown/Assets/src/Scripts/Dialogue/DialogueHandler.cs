﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogueHandler : MonoBehaviour {

    private ShowDialogueOption ShowDialogueOption;
    
    public Dialogue Dialogue;

    void Start () {
        ShowDialogueOption = GetComponent<ShowDialogueOption>();
	}
	
	void Update () {
        if ((ShowDialogueOption.talkable && Input.GetKeyDown(KeyCode.F))  ) {
            PerformDialogue();
        }
	}

    public void PerformDialogue() {
        if(Dialogue.NotBusy)
        StartCoroutine(Dialogue.Perform());
    }

    public void AnswerAction(int answerNumber) {
        Debug.Log("" + Dialogue.Answers.Count);
        if (Dialogue.Answers.ToArray()[answerNumber].OnAnswerEvent != null)
        {
            Dialogue.Answers.ToArray()[answerNumber].OnAnswerEvent.Raise();
        }
        Debug.Log("" + Dialogue.Answers.Count);

        if (Dialogue.Answers.ToArray()[answerNumber].NextDialogue != null)
        {
            Dialogue.DialogueStop();
            Dialogue = Dialogue.Answers.ToArray()[answerNumber].NextDialogue;
            PerformDialogue();
        }
        else {
            Dialogue.DialogueEnded.Raise();
            Dialogue.DialogueStop();
        }
       
    }
}
