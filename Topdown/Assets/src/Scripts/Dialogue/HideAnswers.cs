﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideAnswers : MonoBehaviour {
    public BoolVariable Answer1Visible;
    public BoolVariable Answer2Visible;
    public BoolVariable Answer3Visible;
    public BoolVariable Answer4Visible;


    public void Hide() {
        Answer1Visible.SetValue(false);
        Answer2Visible.SetValue(false);
        Answer3Visible.SetValue(false);
        Answer4Visible.SetValue(false);
    }
}
