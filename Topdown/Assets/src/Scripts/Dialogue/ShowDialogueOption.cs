﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class ShowDialogueOption : MonoBehaviour {

    public bool talkable = false;
    public TextMeshPro Popup;
    public TextMeshPro DialogueGraphics;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (!collider.CompareTag("Player")) return;
        talkable = true;
        Popup.enabled = true;
        DialogueGraphics.enabled = true;
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (!collider.CompareTag("Player")) return;
        talkable = false;
        Popup.enabled = false;
        DialogueGraphics.enabled = false;
    }
}
