﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StringVarOnTMPro : MonoBehaviour {

    public StringVariable message;
    public TextMeshPro TextMeshPro;

    private void Awake()
    {
        message.Value = "";
    }
    void Update () {
        TextMeshPro.text = message.Value;
	}
}