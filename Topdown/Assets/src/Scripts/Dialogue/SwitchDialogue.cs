﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchDialogue : MonoBehaviour {
    public DialogueHandler DialogueHandler;
    public Dialogue Dialogue;

    public void Switch() {
        DialogueHandler.Dialogue = Dialogue;
    }

}
