﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIMonologue : MonoBehaviour
{
    public string[] Messages;
    public TextMeshProUGUI _textMeshProUgui;

    private double _animationTimer = 0;
    [SerializeField] private double _newLetterInterval = 0.025;
    private int _substringLength = 0;

    private int i;
    private bool _sustainingMessage = false;

    public int[] EventTriggeringIndexes;
    public GameEvent[] GameEvents;


    void Start()
    {
        _textMeshProUgui = gameObject.GetComponent<TextMeshProUGUI>();

        i = 0;
        _textMeshProUgui.text = "wykonano start " + Messages.Length + "i=" + i;
    }

    void Update()
    {


        if (_sustainingMessage)
        {
            FadeAnimationAfterClick();
        }
        else if (i < Messages.Length)
        {
            PerformAnimation();
        }
    }

    private void PerformAnimation()
    {
        _animationTimer += Time.deltaTime;
        if (_animationTimer >= _newLetterInterval)
        {
            _animationTimer = 0;
            _textMeshProUgui.text = Messages[i].Substring(0, _substringLength);
            _substringLength++;
        }

        if (_substringLength > Messages[i].Length)
        {
            _sustainingMessage = true;
            _substringLength = 0;
            i++;
        }
    }

    private void FadeAnimationAfterClick()
    {
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.Mouse0))
        {
            _sustainingMessage = false;
            CheckForEvents();
        }
    }

    private void CheckForEvents()
    {
        if (EventTriggeringIndexes == null || GameEvents == null)
        {
            return;
        }

        for (int j = 0; j < EventTriggeringIndexes.Length; j++)
        {
            if (i == EventTriggeringIndexes[j])
            {
                GameEvents[j].Raise();
                break;
            }
        }
    }

    public int GetMessageCounter()
    {
        return i;
    }

}

