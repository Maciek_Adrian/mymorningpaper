﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeDialogWhenCashIsBiggerThan : MonoBehaviour {

    public DialogueHandler DialogueHandler;
    public Dialogue NextDialogue;
    public IntVariable Cash;
    public bool cashok = false;
    public int HowMuchCash = 600;
	

	void Start () {
        StartCoroutine("checkCash");
	}

    public IEnumerator checkCash(){

        while (!cashok) {
            if (Cash.Value >= HowMuchCash) {
                DialogueHandler.Dialogue = NextDialogue;
                cashok = true;
            }

            yield return new WaitForSeconds(1);
        }

        yield return null;
    }


}
