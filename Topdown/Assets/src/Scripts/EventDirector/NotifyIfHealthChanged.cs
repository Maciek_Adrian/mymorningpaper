﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotifyIfHealthChanged : MonoBehaviour {
    public BoolVariable Condition;
    public Health Health;
    private int initialHealth;

	// Use this for initialization
	void Start () {
        initialHealth = Health.HealthPoints;
	}
	
	// Update is called once per frame
	void Update () {
        if (initialHealth != Health.HealthPoints) {
            Condition.SetValue(true);
        }
	}
}
