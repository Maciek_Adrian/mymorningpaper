﻿using UnityEngine;
using Pathfinding;
public class TrustworthyTrap : MonoBehaviour {

    public GameObject Trap;
    public Patrol Patrol;
    public AIDestinationSetter AIDestinationSetter;

    public void SetTheTrap()
    {
        Trap.SetActive(true);
        Patrol.enabled = false;
        AIDestinationSetter.enabled = true;
        Destroy(gameObject, 16);
    }
}
