﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    public string sortingLayerName = string.Empty; //initialization before the methods
    public int orderInLayer = 0;
    public Renderer MyRenderer;
    [SerializeField]
    LayerMask layerMask;
    Vector3 origin;

    private int rayCount = 50;
    private float viewDistance = 10f;
    private float startingAngle;
    private Mesh mesh;
    [SerializeField]private float fov_angle = 90f;



    public float GetFovAngle() {
        return fov_angle;
    }

    public float GetViewDistance() {
        return viewDistance;
    }

    public void SetFovAngle(float angle) {
        fov_angle = angle;
    }

    public void SetViewDistance(float distance) {
        viewDistance = distance;
    }

    private void Start()
    {
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
        SetSortingLayer();
        origin = Vector3.zero;
    }

    private void LateUpdate()
    { 
        
        float angle = startingAngle;
        float angleIncrease = fov_angle / rayCount;

        Vector3[] vertices = new Vector3[rayCount + 1 + 1];
        Vector2[] uv = new Vector2[vertices.Length];
        int[] triangles = new int[rayCount * 3];

        vertices[0] = origin;

        int vertexIndex = 1;
        int triangleIndex = 0;
        for (int i = 0; i <= rayCount; i++)
        {
            Vector3 vertex ;
            RaycastHit2D raycastHit2D = Physics2D.Raycast(origin, MathUtils.GetVectorFromAngle(angle), viewDistance, layerMask);
            if (raycastHit2D.collider == null)
            {
                vertex = origin + MathUtils.GetVectorFromAngle(angle) * viewDistance;
            }
            else {
                vertex = raycastHit2D.point;
            }
            
            vertices[vertexIndex] = vertex;

            if (i > 0)
            {
                triangles[triangleIndex + 0] = 0;
                triangles[triangleIndex + 1] = vertexIndex - 1;
                triangles[triangleIndex + 2] = vertexIndex;

                triangleIndex += 3;
            }
            vertexIndex++;
            angle -= angleIncrease;
        }

        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.triangles = triangles;
        mesh.bounds = new Bounds(origin, Vector3.one * 1000);
    }

    public void SetOrigin(Vector3 origin) {
        this.origin = origin;
    }

    public void SetAimDirection(Vector3 aimDirection) {
        startingAngle = MathUtils.GetAngleFromVectorFloat(aimDirection) + fov_angle /2f;
    }

    public void SetAimDirection(float aimDirection)
    {
        startingAngle = aimDirection + fov_angle / 2f;
    }

    void SetSortingLayer()
    {
        if (sortingLayerName != string.Empty)
        {
            MyRenderer.sortingLayerName = sortingLayerName;
            MyRenderer.sortingOrder = orderInLayer;
        }
    }

}
