﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointService : MonoBehaviour {
    public GameObject CheckPoints;
    public GameObject CurrentCheckPoint;

    public void SetCheckPoint() {
        for (int i = 0; i < CheckPoints.transform.childCount; i++) {
            CheckPoints.transform.GetChild(i).gameObject.active = false;
        }

        CurrentCheckPoint.active = true;
    }
}
