﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsSceneLoader : MonoBehaviour {

    public void LoadScene(int number)
    {

        if (number == 1)
        {
            SceneManager.LoadScene("StartScreen", LoadSceneMode.Single);
        }

    }
}
