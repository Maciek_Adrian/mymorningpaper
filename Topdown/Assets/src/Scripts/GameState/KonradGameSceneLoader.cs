﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class KonradGameSceneLoader : MonoBehaviour
{
    public void Menu()
    {
        SceneManager.LoadScene("StartScreen" , LoadSceneMode.Single);
    }
    public void LoadLevel(int number)
    {
        SceneManager.LoadScene("KonradLevel"+number, LoadSceneMode.Single);
    }

    public void LoadDialogue(int number)
    {
        SceneManager.LoadScene("dialogueScene" + number, LoadSceneMode.Single);
    }
}
