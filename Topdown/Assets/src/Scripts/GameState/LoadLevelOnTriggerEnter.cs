﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadLevelOnTriggerEnter : MonoBehaviour
{
    public KonradGameSceneLoader loader;
    public int DialogueNumber;
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            loader.LoadDialogue(DialogueNumber);
        }
    }
}
