﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenuDropdown : MonoBehaviour {

    public SceneChanger SceneChanger;
    public GameObject WindowLights;
    public GameObject Controls;



    public void PickOption(int option) {
        Controls.SetActive(false);
        if (option == 1 )
        {
            WindowLights.SetActive(true);
            SceneManager.LoadScene("dialogueScene1", LoadSceneMode.Single);
        }
        else if (option == 2)
        {
            Controls.SetActive(true);
        }
        else if (option == 3) {
            Application.Quit();
        }
        
    }
}
