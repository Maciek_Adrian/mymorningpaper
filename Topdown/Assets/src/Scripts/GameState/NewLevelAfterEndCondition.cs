﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewLevelAfterEndCondition : MonoBehaviour
{
    public KonradGameSceneLoader loader;
    public int levelnumber;
    public GameObject WatchedObject;
    // Start is called before the first frame update




    private void Start()
    {
        StartCoroutine("StartLevel");
    }

    private IEnumerator StartLevel()
    {
        while (true)
        {
            if (WatchedObject.active)
            {
                yield return new WaitForSeconds(3);
                loader.LoadDialogue(levelnumber);
            }
            yield return new WaitForSeconds(1);
        }
    }
}
