﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour {

    public BoolVariable GamePaused;
    public GameObject GamePausedImage;

    public void SwitchState() {
        GamePaused.Value = !GamePaused.Value;
    }

    public void PousePlayerMovement() {
        GamePaused.Value = true;
    }

    public void UnPousePlayerMovement() {
        GamePaused.Value = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P)) {
            if (Time.timeScale == 1)
            {
                GamePausedImage.active = true;
                Time.timeScale = 0;
            }
            else {
                Time.timeScale = 1;
                GamePausedImage.active = false;
            }
        }if (Input.GetKeyDown(KeyCode.Q) && Time.timeScale == 0) {
            Application.Quit();
        }
    }
}
