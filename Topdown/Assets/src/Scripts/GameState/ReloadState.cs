﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Pathfinding;

public class ReloadState : MonoBehaviour {

    public GameObject Player;
    public GameObject Enemies;
    public GameObject NPCs;
    public GameObject Camera;

    private GameObject man;

    public BoolVariable[] BoolsTosetFalse;

    public IntVariable Cash;
    public IntVariable Health;
    

    public IntVariable SavedCash;
    public IntVariable SavedHealth;

    public IntVariable PistolAmmo;
    public IntVariable ShotgunAmmo;
    public IntVariable MachinegunAmmo;
    public IntVariable PistolAmmoSaveData;
    public IntVariable ShotgunAmmoSaveData;
    public IntVariable MachinegunAmmoSaveData;

    public void Start()
    {
        LoadState();
    }


    void Update () {
        if (Input.GetKeyDown(KeyCode.R)) { 
            LoadState();
        }
	}

    public void LoadState() {
        Cash.SetValue(SavedCash.Value);
        Health.SetValue(SavedHealth.Value);
        PistolAmmo.SetValue(PistolAmmoSaveData.Value);
        ShotgunAmmo.SetValue(ShotgunAmmoSaveData.Value);
        MachinegunAmmo.SetValue(MachinegunAmmoSaveData.Value);

        for (int i = 0; i < BoolsTosetFalse.Length; i++) {
            BoolsTosetFalse[i].SetValue(false);
        }

        for (int i = 0; i < Player.transform.childCount; i++)
        {
            Destroy(Player.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < Enemies.transform.childCount; i++)
        {
            Enemies.transform.GetChild(i).GetComponent<EnemyAI>().GetEyes().DestroyFieldOfViewInstance();
            Destroy(Enemies.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < NPCs.transform.childCount; i++)
        {
            Enemies.transform.GetChild(i).GetComponent<NPCAi>().GetEyes().DestroyFieldOfViewInstance();
            Destroy(NPCs.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            Transform child = Instantiate(gameObject.transform.GetChild(i));
            child.gameObject.active = true;

            if (child.gameObject.CompareTag("Enemy"))
            {
                child.parent = Enemies.transform;
            }

            if (child.gameObject.CompareTag("npc"))
            {
                child.parent = NPCs.transform;
            }

            if (child.gameObject.CompareTag("Player"))
            {
                child.parent = Player.transform;
                man = child.gameObject;
                SetCamerasOnPlayer(child);
            }
            child.name = gameObject.transform.GetChild(i).name;
        }

        SetEnemiesOnEnemies();
        SetEnemiesOnPlayer();
        SetNpcsOnPlayer();
    }

    public void SetEnemiesOnPlayer() {
        for (int i=0 ; i < Enemies.transform.childCount; i++) {
            Enemies.transform.GetChild(i).GetComponent<EnemyAI>().GetEyes().PlayerTransform =man.transform;
            Enemies.transform.GetChild(i).GetComponent<AIDestinationSetter>().target = man.transform ;
        }
    }

    public void SetEnemiesOnEnemies()
    {
        for (int i = 0; i < Enemies.transform.childCount; i++)
        {
            Enemies.transform.GetChild(i).GetComponent<EnemyAI>().Enemies = Enemies;
        }
    }

    public void SetNpcsOnPlayer()
    {
        for (int i = 0; i < NPCs.transform.childCount; i++)
        {
            Debug.Log("Im setting on player");
            NPCs.transform.GetChild(i).GetComponent<NPCAi>().GetEyes().PlayerTransform = man.transform;
            NPCs.transform.GetChild(i).GetComponent<AIDestinationSetter>().target = man.transform;
        }
    }

    public void SetCamerasOnPlayer(Transform player) {
        for (int i = 0; i < Camera.transform.childCount; i++)
        {
            Camera.transform.GetChild(i).GetComponent<CinemachineVirtualCamera>().Follow = player;
        }
    }
}
