﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavegameScript : MonoBehaviour {

    public GameObject Player;
    public GameObject Npcs;
    public GameObject Enemies;
    public GameObject CheckPoint;

    public IntVariable Cash;
    public IntVariable Health;
    public IntVariable Ammo;

    public IntVariable SavedCash;
    public IntVariable SavedHealth;
    public IntVariable SavedAmmo;


    void Update () {
        if (Input.GetKeyDown(KeyCode.F9)) {
            SaveGame();
        }
	}

    void SaveGame() {
        SavedCash.SetValue(Cash.Value);
        SavedHealth.SetValue(Health.Value);
        SavedAmmo.SetValue(Ammo.Value);

        for (int i = 0; i < CheckPoint.transform.childCount; i++)
        {
            Destroy(CheckPoint.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < Player.transform.childCount; i++)
        {
            Transform child = Instantiate(Player.transform.GetChild(i));
            child.gameObject.active = false;
            child.parent = CheckPoint.transform;
            child.name = Player.transform.GetChild(i).name;
        }

        for (int i = 0; i < Enemies.transform.childCount; i++)
        {
            Transform child = Instantiate(Enemies.transform.GetChild(i));
            child.gameObject.active = false;
            child.parent = CheckPoint.transform;
            child.name = Enemies.transform.GetChild(i).name;
        }

        for (int i = 0; i < Npcs.transform.childCount; i++)
        {
            Transform child = Instantiate(Npcs.transform.GetChild(i));
            child.gameObject.active = false;
            child.parent = CheckPoint.transform;
            child.name = Npcs.transform.GetChild(i).name;
        }
    }
}
