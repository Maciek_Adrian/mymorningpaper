﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour {

    public void LoadScene(int number) {
        if (number == 0) {
            SceneManager.LoadScene("StartScreen", LoadSceneMode.Single);
        }
        if (number == 1)
        {
            SceneManager.LoadScene("MVPCyberPunkScene", LoadSceneMode.Single);
        }
        else if (number == 2) {
            SceneManager.LoadScene("CreditsScreen", LoadSceneMode.Single);
        }
    }
}
