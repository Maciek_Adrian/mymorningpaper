﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float moveSpeed = 5f;
    public Rigidbody2D RigidB;
    public Animator animator;
    public BoolVariable GamePaused;

    public Vector2 movement;

    void Update () {
        if (GamePaused.Value) return;

        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", movement.sqrMagnitude);

    }

    private void FixedUpdate()
    {
        if (GamePaused.Value) return;

        movement.Normalize();
        RigidB.MovePosition(RigidB.position + movement * moveSpeed * Time.fixedDeltaTime);

        
    }
}
