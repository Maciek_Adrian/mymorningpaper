﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementDS : MonoBehaviour {

    [SerializeField] private Animator animator;

    public float moveSpeed = 5f;

    public Rigidbody2D rb;
    public Camera cam;

    Vector2 movement;
    Vector2 MousePos;

    public FieldOfView FieldOfView;

    public BoolVariable GamePaused;

	void Update () {
        if (GamePaused.Value) return;
        movement.x = Input.GetAxis("Horizontal");
        movement.y = Input.GetAxis("Vertical");

        animator.SetBool("moving", movement.magnitude > 0);

        MousePos = cam.ScreenToWorldPoint(Input.mousePosition);
    }

    private void FixedUpdate()
    {
        if (GamePaused.Value) return;
        movement.Normalize();
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);

        Vector2 lookDir = MousePos - rb.position;
        lookDir.Normalize();
        float angle = Mathf.Atan2(lookDir.y,lookDir.x)* Mathf.Rad2Deg ;
        rb.rotation = angle;
        FieldOfView.SetAimDirection(lookDir);
        FieldOfView.SetOrigin(transform.position);

    }
}
