﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Answer{

    public string text;
    public Dialogue NextDialogue;
    public GameEvent OnAnswerEvent;
    public BoolVariable AnswerVisible;
    public StringVariable ButtonText;

    public void RaiseGameEvent() {
        OnAnswerEvent.Raise();
    }

}
