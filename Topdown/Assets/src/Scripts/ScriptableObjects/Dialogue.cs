﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dialogue")]
public class Dialogue : ScriptableObject {

    public List<string> NPCTexts;
    public List<Answer> Answers;
    public StringVariable message;
    public int messageNumber = 0;
    private int letterNumber = 0;

    public bool NotBusy = true;
    public bool DialogueInProgress = false;

    public GameEvent DialogueStarted;
    public GameEvent DialogueEnded;

    public IEnumerator Perform() {
        NotBusy = false;
        if (!DialogueInProgress) {
            DialogueStart();
            DialogueStarted.Raise();
        }

        if (messageNumber < NPCTexts.ToArray().Length)
        {
            while (letterNumber <= NPCTexts.ToArray()[messageNumber].Length)
            {
                message.SetValue(NPCTexts.ToArray()[messageNumber].Substring(0, letterNumber));
                letterNumber++;
                yield return new WaitForSeconds(0.02f);
            }
            letterNumber = 0;
            messageNumber++;
        }
        if (messageNumber == NPCTexts.Count) { 
            messageNumber = 0;
            if (Answers.Count == 0)
            {

                DialogueStop();
                DialogueEnded.Raise();
            }
            else
            {
                foreach (Answer answer in Answers) {
                    answer.AnswerVisible.Value = true;
                    answer.ButtonText.SetValue(answer.text);
                }
            }
        }
        NotBusy = true;
        yield return null;
    }

    public void SetMessageNumber(int number) {
        messageNumber = number;
    }

    public void DialogueStop() {
        DialogueInProgress = false;
    }

    public void DialogueStart() {
        DialogueInProgress = true;
    }

}