﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableObjectsSetup : MonoBehaviour {
    public List<BoolVariable> BoolVariablesSetTrue;
    public List<BoolVariable> BoolVariablesSetFalse;
    public List<StringVariable> StringVariablesSetEmpty;

    public List<Dialogue> SetDialoguesToBegining;
    public WeaponVariable Weapon;
    public GunType StartingWeapon;
    public IntVariable PistolAmmo;
    public IntVariable ShotgunAmmo;
    public IntVariable MachinegunAmmo;
    public IntVariable PistolAmmoSaveData;
    public IntVariable ShotgunAmmoSaveData;
    public IntVariable MachinegunAmmoSaveData;


    public IntVariable Cash;
    public IntVariable SavedCash;
    
    public int initialPistol;
    public int initialShotgun;
    public int initialMachineGun;

    public int initialCash;



    // Use this for initialization
    void Start () {
        Weapon.SetValue(StartingWeapon);
        PistolAmmo.SetValue(initialPistol);
        ShotgunAmmo.SetValue(initialShotgun);
        MachinegunAmmo.SetValue(initialMachineGun);

        PistolAmmoSaveData.SetValue(initialPistol);
        ShotgunAmmoSaveData.SetValue(initialShotgun);
        MachinegunAmmoSaveData.SetValue(initialMachineGun);

        Cash.SetValue(initialCash);
        SavedCash.SetValue(initialCash);

        for (int i = 0; i < BoolVariablesSetFalse.Count; i++) {
            BoolVariablesSetFalse.ToArray()[i].Value = false;
        }
        for (int i = 0; i < BoolVariablesSetTrue.Count; i++)
        {
            BoolVariablesSetTrue.ToArray()[i].Value = false;
        }
        for (int i = 0; i < StringVariablesSetEmpty.Count; i++)
        {
            StringVariablesSetEmpty.ToArray()[i].Value = "";
        }
        for (int i = 0; i < SetDialoguesToBegining.Count; i++)
        {
            SetDialoguesToBegining.ToArray()[i].SetMessageNumber(0);
            SetDialoguesToBegining.ToArray()[i].NotBusy = true;
            SetDialoguesToBegining.ToArray()[i].DialogueInProgress = false;

        }
    }

}
