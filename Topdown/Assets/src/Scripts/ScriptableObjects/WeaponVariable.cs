﻿using UnityEngine;


[CreateAssetMenu(menuName="Variables/WeaponEnumVariable")]
public class WeaponVariable : ScriptableObject
{
#if UNITY_EDITOR
        [Multiline]
        public string DeveloperDescription = "";
#endif
        public GunType Value;

        public void SetValue(GunType value)
        {
            Value = value;
        }

        public void SetValue(WeaponVariable value)
        {
            Value = value.Value;
        }

        public void ApplyChange(GunType change)
        {
            Value = change;
        }

        public void ApplyChange(WeaponVariable change)
        {
            Value = change.Value;
        }
}

