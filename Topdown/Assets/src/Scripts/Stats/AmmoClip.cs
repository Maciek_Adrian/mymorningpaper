﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoClip : MonoBehaviour {

    public IntVariable AmmoQuantity;


    public void AddAmmo(int amount)
    {
        AmmoQuantity.SetValue(AmmoQuantity.Value + amount);
    }

    public void RemoveAmmo(int amount)
    {
        AmmoQuantity.SetValue(AmmoQuantity.Value - amount);
    }
}
