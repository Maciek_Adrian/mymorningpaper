﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cash : MonoBehaviour {
    public IntVariable CashVariable;


    public void AddCash(int amount) {
        CashVariable.SetValue(CashVariable.Value + amount);
    }

    public void RemoveCash(int amount)
    {
        CashVariable.SetValue(CashVariable.Value - amount);
    }
}
