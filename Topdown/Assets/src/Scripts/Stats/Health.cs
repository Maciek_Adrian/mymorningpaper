﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    public int HealthPoints = 1;
    public int ArmorPoints = 0;
    public IntVariable UIHp;
    public IntVariable UIArmor;

    private void Start()
    {
        SetUi();
    }

    public void SetUi() {
        if (UIHp != null)
        {
            UIHp.SetValue(HealthPoints);
        }
        if (UIArmor != null)
        {
            UIArmor.SetValue(ArmorPoints);
        }
    }

    public bool PistolHit() {

        if (ArmorPoints > 0)
        {
            ArmorPoints -= 1;
            SetUi();
            return false;
        }

        HealthPoints -= 1;
        SetUi();
        if (HealthPoints > 0)
        { 
            return false;
        }

        return true;
    }

    public bool MeleeHit() {
        HealthPoints -= 1;
        SetUi();
        if (HealthPoints > 0)
        { 
            return false;
        }

        return true;
    }
}
