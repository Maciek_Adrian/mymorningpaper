﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowCircles : MonoBehaviour {
    public Health health;
	
	
	// Update is called once per frame
	void Update () {
        for (int i = 0; i < gameObject.transform.childCount; i++) {
            if (i < health.HealthPoints)
            {
                gameObject.transform.GetChild(i).gameObject.active = true;
            }
            else {
                gameObject.transform.GetChild(i).gameObject.active = false;
            }
        }
	}
}
