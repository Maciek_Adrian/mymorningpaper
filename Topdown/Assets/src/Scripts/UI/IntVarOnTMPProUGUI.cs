﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class IntVarOnTMPProUGUI : MonoBehaviour {


    public IntVariable message;
    public TextMeshProUGUI TextMeshPro;

    void Update()
    {
        TextMeshPro.text = ""+message.Value;
    }

}
