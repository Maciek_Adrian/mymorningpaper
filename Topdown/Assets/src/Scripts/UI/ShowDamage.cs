﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowDamage : MonoBehaviour {

    public int lasthealth;
    public IntVariable Health;
    public GameObject AnimateDamage;

	void Start () {
        lasthealth = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (lasthealth > Health.Value)
        { 
            StopAllCoroutines();
            StartCoroutine("DamageAnim");
        }
        lasthealth = Health.Value;
    }

    IEnumerator DamageAnim() {

        AnimateDamage.SetActive(true);
        Debug.Log("Animating");
        yield return new WaitForSeconds(0.5f);
        Debug.Log("AndNownot");
        AnimateDamage.SetActive(false);
        yield return null;
    }
}
