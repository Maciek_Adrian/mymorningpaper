﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowFraggedMessage : MonoBehaviour {

    public GameObject FraggedObject;
    public GameObject Player;
	// Update is called once per frame
	void Update () {
        if (Player.transform.childCount == 0)
        {
            FraggedObject.SetActive(true);
        }
        else if (FraggedObject.active) {
            FraggedObject.SetActive(false);
        }
	}
}
