﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowWeapon : MonoBehaviour
{
    [SerializeField] private WeaponVariable weaponVariable;
    private GunType previousWeapon;


    void Update()
    {
        if (weaponVariable.Value == previousWeapon) return;

        previousWeapon = weaponVariable.Value;
        showWeaponSprite();
    }

    private void showWeaponSprite() { 
        for(int i=0; i< gameObject.transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        switch (weaponVariable.Value)
        {
            case GunType.FIST:
                transform.GetChild(0).gameObject.SetActive(true);
                break;
            case GunType.MELEE:
                transform.GetChild(1).gameObject.SetActive(true);
                break;
            case GunType.HANDGUN:
                transform.GetChild(2).gameObject.SetActive(true);
                break;
            case GunType.SHOTGUN:
                transform.GetChild(3).gameObject.SetActive(true);
                break;
            case GunType.MACHINEGUN:
                transform.GetChild(4).gameObject.SetActive(true);
                break;

        }
    }
}
