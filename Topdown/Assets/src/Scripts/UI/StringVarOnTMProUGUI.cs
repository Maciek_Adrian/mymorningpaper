﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StringVarOnTMProUGUI : MonoBehaviour {

    public StringVariable message;
    public TextMeshProUGUI TextMeshPro;

    void Update()
    {
        TextMeshPro.text = message.Value;
    }
}
