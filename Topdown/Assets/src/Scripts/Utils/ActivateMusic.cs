﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateMusic : MonoBehaviour {
    public SwitchSoundTracks SwitchTracks;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player")) return;

        SwitchTracks.SwitchToFighting();
    }
}
