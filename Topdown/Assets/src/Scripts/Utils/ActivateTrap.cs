﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class ActivateTrap : MonoBehaviour {

    BoxCollider2D trigger;
    public GameObject Enemies;
    public GameObject Player;
    public GameObject EnemiesOriginObject;

    // Use this for initialization
    void Start () {
        trigger = GetComponent<BoxCollider2D>();
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player")) return;
        AddEnemies();
        trigger.enabled = false;
    }

    public void SetTrap() {
        trigger.enabled = true;
    }

    public void AddEnemies()
    {
        for (int i = 0; i < EnemiesOriginObject.transform.childCount; i++)
        {
            Transform child = Instantiate(EnemiesOriginObject.transform.GetChild(i));
            child.gameObject.SetActive(true);
            child.parent = Enemies.transform;
            child.GetComponent<EnemyAI>().GetEyes().PlayerTransform = Player.transform.GetChild(0);
            child.GetComponent<AIDestinationSetter>().target = Player.transform.GetChild(0);
        }
    }
}
