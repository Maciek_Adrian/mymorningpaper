﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivationListener : MonoBehaviour {
    public BoolVariable Flag;
    public GameObject WatchedObject;

    private bool LastValueOfFlag;

    void Update () {
        if (Flag.Value != LastValueOfFlag) {
            LastValueOfFlag = Flag.Value;
            WatchedObject.active = Flag.Value;
        }
	}
}
