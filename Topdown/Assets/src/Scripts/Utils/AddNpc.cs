﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddNpc : MonoBehaviour {
    public GameObject Npc;
    public GameObject Npcs;

    public void AddNpcs() {
        GameObject nonplayer = Instantiate(Npc);
        //nonplayer.transform.position = gameObject.transform.position;
        nonplayer.SetActive(true);
        nonplayer.transform.parent = Npcs.transform;
        nonplayer.name = Npc.transform.name;

    }

}
