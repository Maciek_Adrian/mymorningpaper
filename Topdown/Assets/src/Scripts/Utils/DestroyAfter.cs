﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfter : MonoBehaviour {

    public float destroyTime = 10;
	void Start () {
        Destroy(gameObject, destroyTime);
	}
	

}
