﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillObjectInstantly : MonoBehaviour {
    public GameObject BloodEffect;

    public void Kill() {
        GameObject effect = Instantiate(BloodEffect, transform.position, Quaternion.identity);
        Destroy(effect, 0.5f);

        Destroy(gameObject);
    }

}
