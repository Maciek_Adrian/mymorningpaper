﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetObjectTransform : MonoBehaviour {

    public GameObject ObjectToChange;
    public GameObject DesiredTransform;


    public void PerformChange() {
        ObjectToChange.transform.position = DesiredTransform.transform.position;
    }

}
