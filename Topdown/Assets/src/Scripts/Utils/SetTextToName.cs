﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SetTextToName : MonoBehaviour {
    public GameObject ParentObj;
    TextMeshPro tmPro;


	// Use this for initialization
	void Start () {
        tmPro = gameObject.GetComponent<TextMeshPro>();
        tmPro.text = ParentObj.name;
	}
	
}
