﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchSoundTracks : MonoBehaviour {

    public AudioSource BackGroundMusic;
    public AudioSource FightingMusic;

    public void SwitchToFighting() {
        BackGroundMusic.enabled = false;
        FightingMusic.enabled = true;
    }

}
