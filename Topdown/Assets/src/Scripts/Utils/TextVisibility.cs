﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextVisibility : MonoBehaviour {

    public DialogueHandler DialogueHandler;
    public Animator Anim;

    private void Start()
    {
        StartCoroutine("CheckTextVisibility");
    }

    private IEnumerator CheckTextVisibility() {
        while (true) {
            if (DialogueHandler.Dialogue.DialogueInProgress)
            {
                Anim.SetBool("textVisibility", true);
            }
            else {
                Anim.SetBool("textVisibility", false);
            }
            yield return new WaitForSeconds(0.1f);
        }
    }


}
