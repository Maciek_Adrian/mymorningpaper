﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableGun : MonoBehaviour {

    public int AmmoQuantity = 8;
    public IntVariable AmmoVariable;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")) {
            AmmoVariable.ApplyChange(AmmoQuantity);
            Destroy(gameObject);
        }
    }
}
