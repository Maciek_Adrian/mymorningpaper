﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableMedpack : MonoBehaviour {

    public int HealthQuantity = 2;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.GetComponent<Health>().HealthPoints += HealthQuantity;
            collision.GetComponent<Health>().SetUi();

            Destroy(gameObject);
        }
    }
}
