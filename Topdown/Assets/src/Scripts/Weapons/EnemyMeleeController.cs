﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMeleeController : MonoBehaviour
{
    public Animator anim;
    public CapsuleCollider2D hitrange;
    public void Swing()
    {
        anim.SetBool("Swing", true);
        hitrange.enabled = true;
        StartCoroutine("EndSwing");
    }

    private IEnumerator EndSwing()
    {
        yield return new WaitForSeconds(0.25f);
        anim.SetBool("Swing", false);
        hitrange.enabled = false;
        yield return null;
    }
}
