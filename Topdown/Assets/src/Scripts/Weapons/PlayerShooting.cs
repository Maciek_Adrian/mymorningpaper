﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour {

    public Transform FirePoint;
    public GameObject bulletPrefab;
    public AmmoClip PistolClip;
    public AmmoClip ShotgunClip;
    public AmmoClip MachineGunClip;
    public BoolVariable GamePaused;

    public float bulletForce = 20f;

    [SerializeField] private WeaponVariable Gun;
    public float reloadTime = 0.2f;
    public float timer = 0;
    private bool reloaded = true;
	
	// Update is called once per frame
	void Update () {
        if (GamePaused.Value) return;

        timer += Time.deltaTime;
        if (timer > reloadTime) {
            timer = 0;
            reloaded = true;
        }


        if (Input.GetButton("Fire1") && reloaded) {
            Shoot();
            reloaded = false;
        }
	}

    void Shoot() {

        switch (Gun.Value) {
            case GunType.HANDGUN:
                handgunShot();
                break;
            case GunType.SHOTGUN:
                shotgunShot();
                break;
            case GunType.MACHINEGUN:
                machinegunShot();
                break;
        }


    }

    private void handgunShot() {
        if (PistolClip != null)
        {
            if (PistolClip.AmmoQuantity.Value <= 0)
            {
                return;
            }
            else
            {
                PistolClip.RemoveAmmo(1);
            }
        }

        GameObject bullet = Instantiate(bulletPrefab, FirePoint.position, FirePoint.rotation);

        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(FirePoint.right * bulletForce, ForceMode2D.Impulse);
    }

    private void shotgunShot() {
        if (ShotgunClip != null)
        {
            if (ShotgunClip.AmmoQuantity.Value <= 0)
            {
                return;
            }
            else
            {
                ShotgunClip.RemoveAmmo(1);
            }
        }

        for (int i = 0; i < 5; i++)
        {
            GameObject bullet = Instantiate(bulletPrefab, FirePoint.position+FirePoint.right*i/4, FirePoint.rotation);
            bullet.transform.Rotate(new Vector3(0,0,-10+i*5));

            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(bullet.transform.right * bulletForce, ForceMode2D.Impulse);
        }
    }

    private void machinegunShot()
    {
        if (MachineGunClip != null)
        {
            if (MachineGunClip.AmmoQuantity.Value <= 0)
            {
                return;
            }
            else
            {
                MachineGunClip.RemoveAmmo(1);
            }
        }

        GameObject bullet = Instantiate(bulletPrefab, FirePoint.position, FirePoint.rotation);

        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(FirePoint.right * bulletForce, ForceMode2D.Impulse);
    }

    public void ChangeGun(GunType gunType)
    {
        Gun.SetValue(gunType);
    }
}
