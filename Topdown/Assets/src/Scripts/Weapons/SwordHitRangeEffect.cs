﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordHitRangeEffect : MonoBehaviour {
    public GameObject BloodEffect;
    public GameObject[] Collectables;

    private void OnTriggerEnter2D(Collider2D collision)
    {
         if (collision.gameObject.layer == LayerMask.NameToLayer("People"))
        {
            if (collision.GetComponent<Health>().MeleeHit())
            {
                int drop = Random.Range(0, 10);
                 if (drop > 7)
                {
                    GameObject treasure = Instantiate(Collectables[1]);
                    treasure.transform.position = gameObject.transform.position;
                }

                if (collision.GetComponent<EnemyAI>() != null)
                {
                    collision.GetComponent<EnemyAI>().GetEyes().DestroyFieldOfViewInstance();
                    if (collision.GetComponent<EnemyAI>().Loot != null)
                    {
                        GameObject loot = Instantiate(collision.GetComponent<EnemyAI>().Loot);
                        loot.transform.position = gameObject.transform.position;
                    }


                }
                if (collision.GetComponent<NPCAi>() != null)
                {
                    collision.GetComponent<NPCAi>().GetEyes().DestroyFieldOfViewInstance();
                }
                Destroy(collision.gameObject);
            }
            GameObject effect = Instantiate(BloodEffect, transform.position, Quaternion.identity);
            Destroy(effect, 0.5f);

        }
    }

}
