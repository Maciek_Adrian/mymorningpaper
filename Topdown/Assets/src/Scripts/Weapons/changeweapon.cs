﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeweapon : MonoBehaviour
{
    katana sword;
    public GameObject swordgraphx;
    public GameObject GunGrapx;
    PlayerShooting shooting;


    void Start()
    {
        sword = GetComponent<katana>();
        shooting = GetComponent<PlayerShooting>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            GunGrapx.SetActive(false);
            sword.enabled = false;
            shooting.enabled = false;
            swordgraphx.active = false;
            shooting.ChangeGun(GunType.FIST);

        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            shooting.enabled = false;
            GunGrapx.SetActive(false);
            sword.enabled = true;
            swordgraphx.active = true;
            shooting.ChangeGun(GunType.MELEE);

        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            sword.enabled = false;
            shooting.enabled = true;
            GunGrapx.SetActive(true);
            swordgraphx.active = false;
            shooting.ChangeGun(GunType.HANDGUN);
            shooting.reloadTime = 0.5f;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            sword.enabled = false;
            shooting.enabled = true;
            GunGrapx.SetActive(true);
            swordgraphx.active = false;
            shooting.ChangeGun(GunType.SHOTGUN);
            shooting.reloadTime = 1f;
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            sword.enabled = false;
            shooting.enabled = true;
            GunGrapx.SetActive(true);
            swordgraphx.active = false;
            shooting.ChangeGun(GunType.MACHINEGUN);
            shooting.reloadTime = 0.2f;
        }

    }
}
