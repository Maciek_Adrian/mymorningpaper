﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class katana : MonoBehaviour {
    public Animator anim;
    public CapsuleCollider2D hitrange;
    public BoolVariable GamePaused;

	void Update () {
        if (GamePaused.Value) return;
        if (Input.GetButtonDown("Fire1"))
        {
            Swing();
        }

    }

    void Swing()
    {
        anim.SetBool("Swing", true);
        hitrange.enabled = true;
        StartCoroutine("EndSwing");
    }

    private IEnumerator EndSwing() {
        yield return new WaitForSeconds(0.25f);
        anim.SetBool("Swing", false);
        hitrange.enabled = false;
        yield return null;
    }
}
